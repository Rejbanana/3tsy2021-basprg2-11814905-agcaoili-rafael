#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>


using namespace std;

class Spell {

 public:
	string name;
	int manaCost;
	int damage;
	int minDmg, maxDmg;

	void getDamage(Spell* spell) {
	
		this->damage = this->minDmg + (rand() % (this->maxDmg - this->minDmg) + 1);
	}
};

class Attack {

 public:
	string name;
	int manaGen;
	int damage;
	int minDmg, maxDmg;
	int manaGenMin, manaGenMax;

	void getDamageAndManaGen(Attack* attack) {
	
		
		this->damage = rand() % (this->maxDmg - this->minDmg) + this->minDmg + 1;
		this->manaGen = rand() % (this->manaGenMin - this->manaGenMax) + this->manaGenMin + 1;
	}

};

class Wizard {

 public:
	string name;
	int hp;
	int mana;

	void castSpell(Wizard* target, Spell* spell) {
		cout << this->name << " casts " << spell->name << " to attack " << target->name << endl;
		cout << this->name << " deals " << spell->damage << "!" << endl;
		this->mana -= spell->manaCost;
		target->hp -= spell->damage;
		_getch();
	}
	void castAttack(Wizard* target, Attack* attack) {

		cout << this->name << " uses " << attack->name << " to attack " << target->name << endl;
		cout << this->name << " deals " << attack->damage << "!" << endl;
		this->mana += attack->manaGen;
		target->hp -= attack->damage;
		_getch();
	}
};

int main()
{
	srand(time(NULL));

	Wizard* wiz1 = new Wizard;
	wiz1->hp = 250;
	wiz1->mana = 0;

	Wizard* wiz2 = new Wizard;
	wiz2->hp = 250;
	wiz2->mana = 0;

	Spell* fireball = new Spell;
	fireball->name = "Fireball";
	fireball->manaCost = 50;
	fireball->minDmg = 40;
	fireball->maxDmg = 60;

	Attack* attack = new Attack;
	attack->name = "melee";
	attack->manaGen;
	attack->manaGenMin = 10;
	attack->manaGenMax = 20;
	attack->minDmg = 10;
	attack->maxDmg = 15;

	cout << "Enter the name of the first wizard: ";
	cin >> wiz1->name;
	cout << "Enter the name of the second wizard: ";
	cin >> wiz2->name;

	int firstAtk = rand() % 2 + 1;

	if (firstAtk == 1) {
	
		cout << wiz1->name << " attacks first!\n";
	}
	else {
	
		cout << wiz2->name << " attacks first!\n";
	}
	cout << endl;
	_getch();

	while (wiz1->hp > 0 && wiz2->hp > 0) {
	
		cout << wiz1->name << endl;
		cout << "HP: " << wiz1->hp << endl;
		cout << "Mana: " << wiz1->mana << endl;
		cout << wiz2->name << endl;
		cout << "HP: " << wiz2->hp << endl;
		cout << "Mana: " << wiz2->mana << endl;

		cout << endl;
		_getch();
		
		if (firstAtk == 1) {

			if (wiz1->mana >= 50) {
				fireball->getDamage(fireball);
				wiz1->castSpell(wiz2, fireball);
			}
			else if (wiz1->mana < 50) {
				attack->getDamageAndManaGen(attack);
				wiz1->castAttack(wiz2, attack);
			}

			firstAtk++;
		}
		else {
			if (wiz2->mana >= 50) {
				fireball->getDamage(fireball);
				wiz2->castSpell(wiz1, fireball);
			}
			else if (wiz1->mana < 50) {
				attack->getDamageAndManaGen(attack);
				wiz2->castAttack(wiz1, attack);
			}
			firstAtk--;
		}

		cout << endl;
		system("cls");
	}

	if (wiz1->hp > 0 && wiz2->hp <= 0) {
	
		cout << wiz2->name << " has been defeated!\n";
		cout << wiz1->name << " wins the match!\n";
	}
	else if (wiz1->hp <= 0 && wiz2->hp > 0) {
	
		cout << wiz1->name << " has been defeated!\n";
		cout << wiz2->name << " wins the match!\n";
	}
	cout << endl << "Thank you for playing!";
	_getch();
	return 0;
}