#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>
#include "Node.h"

using namespace std;


Node* CreateNodes(int size) { //creates the linked list
    Node* head = new Node;
    Node* prev = head;

    for (int i = 1; i < size; i++) {

        Node* nextNode = new Node; //creates a node to add to the list
        prev->next = nextNode;
        prev = prev->next;
    }
    prev->next = head;//links the last node to the first node
    return head;
}

Node* AddNames(Node* members, string input) { //adds names to the members

    members->name = input;
    members = members->next;

    return members;
}

void PrintNames(Node* members, int size) { //prints names

    for (int i = 0; i < size; i++) {
    
        cout << members->name << endl;
        members = members->next;
    }
}


Node* PickStartingMember(Node* members, int startNum) {// picks the starting member

    for (int i = 0; i < startNum; i++) {
    
        members = members->next;
    }
    return members;
}

void GetAndPrintElim(Node* members, int elimNum) {//get and prints the eliminated member

    Node* temp = members;
    for (int i = 0; i < elimNum; i++) {
    
        temp = temp->next;
    }
    cout << temp->name << " was eliminated. " << endl;
}

Node* eliminateMember(Node* members, int elimNum) { //eliminates the member and patches the link

    Node* prev = new Node;

    for (int i = 0; i < elimNum; i++) {
    
        prev = members; //holds the value of the previous member in the list
        members = members->next;
    }

    prev->next = members->next; //patches the link
    delete members; //eliminates the member/node
    prev = prev->next; //assigns the current member to the right cloak holder
    return prev; //passes the modified linked list
}

int main() {

    srand(time(NULL));

    int playerSize;
    int round = 1;
    Node* members = new Node;

    cout << "Enter the amount of members: " << endl;
    cin >> playerSize; // gets the size of the members
    members = CreateNodes(playerSize); // creates the list with the amount of members

    string name;
    for (int i = 0; i < playerSize; i++) { //Adds the names for every member
    
        cout << "What's your name? ";
        cin >> name;
        members = AddNames(members, name);
    }

    int startNum = rand() % playerSize + 1;
    members = PickStartingMember(members, startNum);//draws for the starting member
    cout << endl;

    while (playerSize > 1) { //loop until 1 player is left
    
        cout << "=================================================\n";
        cout << "ROUND " << round << endl;
        cout << "=================================================\n";
        cout << "Remaining Members: \n";
        PrintNames(members, playerSize); //prints names
        cout << endl;

        _getch();
        int elimNum = rand() % playerSize + 1; //draws for eliminated member
        cout << "Result: " << endl;
        _getch();
        cout << members->name << " has drawn " << elimNum << endl;
        GetAndPrintElim(members, elimNum); //prints the eliminated person
        members = eliminateMember(members, elimNum); //takes the eliminated member off the list

        playerSize--; //reduces the size for the loop
        round++; //increments the round

        system("pause");
        cout << endl;
    }

    cout << "=================================================\n";
    cout << "FINAL RESULT" << endl;
    cout << "=================================================\n";
    _getch();
    cout << members->name << " will go to seek reinforcements!" << endl; //prints final result
    _getch();

    return 0;
}