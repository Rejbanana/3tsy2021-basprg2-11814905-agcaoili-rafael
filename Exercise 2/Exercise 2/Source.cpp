#include <iostream>
#include <string>
#include <time.h>
#include <vector> 

using namespace std; 


void printVector(const vector<string>& vector)
{
	for (int i = 0; i < vector.size(); i++)
	{
		cout << vector[i] << endl;
	}
}


void populateVector(vector<string>& vector){

	string items[4] = { "RedPotion", "Elixir", "EmptyBottle", "BluePotion" };
	for (int i = 0; i < 10; i++)
	{
		vector.push_back(items[(rand() % 4)]);

	}
}


void countVector(const vector<string>& vector) {
	int item1 = 0, item2 = 0, item3 = 0, item4 = 0;
	for (int i = 0; i < 10; i++) {

		if (vector[i] == "RedPotion") {
			item1++;
		}
		else if (vector[i] == "Elixir") {
			item2++;
		}
		else if (vector[i] == "EmptyBottle") {
			item3++;
		}
		else if (vector[i] == "BluePotion") {
			item4++;
		}

	}

	cout << "Red Potion" << " == " << item1 << endl;
	cout << "Elixir" << " == " << item2 << endl;
	cout << "Empty Bottle" << " == " << item3 << endl;
	cout << "Blue Potion" << " == " << item4 << endl;
}

void deleteVector(vector<string>& vector) {
	int slot = 0;
	while (slot < 1 or slot > 10) {
		cout << "enter an Inventory Slot to Delete" << endl;
		cin >> slot;
	}
	vector.erase(vector.begin() + (slot - 1));
}

int main()
{

	srand(time(NULL));

	
	vector<string> inventory;
	
	
	
	populateVector(inventory);
	printVector(inventory);
	cout << endl;
	countVector(inventory);
	cout << endl;
	deleteVector(inventory);
	cout << endl;
	printVector(inventory);
	

	system("pause");
	return 0;
}
