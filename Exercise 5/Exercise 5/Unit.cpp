#include "Unit.h"

#include "Skill.h"
#include "Heal.h"
#include "Might.h"
#include "IronSkin.h"
#include "Concentration.h"
#include "Haste.h"

Unit::Unit(string name)
{
	this->playerName = name;
	this->hp = 10;
	this->pow = 0;
	this->vit = 0;
	this->dex = 0;
	this->agi = 0;

	this->addSkill(new Heal());
	this->addSkill(new Might());
	this->addSkill(new IronSkin());
	this->addSkill(new Concentration());
	this->addSkill(new Haste());
}

Unit::~Unit()
{
}

void Unit::printStats()
{
	cout << "Name: " << this->playerName << endl;
	cout << "HP: " << this->hp << endl;
	cout << "Pow: " << this->pow << endl;
	cout << "Vit: " << this->vit << endl;
	cout << "Dex: " << this->dex << endl;
	cout << "Agi: " << this->agi << endl;
}

void Unit::addSkill(Skill* skill)
{
	this->skillSet.push_back(skill);
}


void Unit::castRandSkill()
{
	int randSkillNum = rand() % this->skillSet.size();

	for (int i = 0; i < this->skillSet.size(); i++){
		if (i == randSkillNum){
			cout << this->playerName << " used " << skillSet[i]->skillName << "!" << endl;
			skillSet[i]->activate(this);
		}
	}
}
