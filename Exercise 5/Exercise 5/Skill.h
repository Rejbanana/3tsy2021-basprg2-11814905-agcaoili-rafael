#pragma once
#include <string>
#include <iostream>
#include "Unit.h"

using namespace std;

class Skill
{
public:
	Skill();
	~Skill();

	virtual void activate(Unit* caster);

	string skillName;
};

