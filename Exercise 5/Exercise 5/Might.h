#pragma once
#include "Skill.h"

class Might : public Skill
{
public:
	Might();
	~Might();

	void activate(Unit* caster);
};

