#include "IronSkin.h"

IronSkin::IronSkin()
{
	this->skillName = "Iron Skin";
}

IronSkin::~IronSkin()
{
}

void IronSkin::activate(Unit* caster)
{
	int vitAmount = 2;

	caster->vit += vitAmount;

	cout << "Vitality increased by " << vitAmount << "!" << endl;
}