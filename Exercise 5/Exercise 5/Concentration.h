#pragma once
#include "Skill.h"
class Concentration : public Skill
{
public:
	Concentration();
	~Concentration();
	
	void activate(Unit* caster);
};

