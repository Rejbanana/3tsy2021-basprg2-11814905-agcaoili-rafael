#pragma once
#include <string>
#include <iostream>
#include <vector>
//#include "Skill.h"

using namespace std;
class Skill;
class Unit
{
public: 
	Unit(string name);
	~Unit();

	string playerName;
	int hp;
	int pow;
	int vit;
	int dex;
	int agi;
	vector<Skill*> skillSet;

	void printStats();
	void addSkill(Skill* skill);
	void castRandSkill();
};

