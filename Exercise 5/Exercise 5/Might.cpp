#include "Might.h"

Might::Might()
{
	this->skillName = "Might";
}

Might::~Might()
{
}

void Might::activate(Unit* caster)
{
	int powAmount = 2;

	caster->pow += powAmount;

	cout << "Power increased by " << powAmount << "!" << endl;
}
