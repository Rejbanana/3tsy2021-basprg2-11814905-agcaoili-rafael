#include "Heal.h"

Heal::Heal()
{
	this->skillName = "Heal";
}

Heal::~Heal()
{
}

void Heal::activate(Unit* caster)
{
	int hpHeal = 10;

	caster->hp += 10;
	
	cout << "HP healed by " << hpHeal << "!" << endl;
}
