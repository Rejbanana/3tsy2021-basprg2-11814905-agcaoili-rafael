#include "Concentration.h"

Concentration::Concentration()
{
	this->skillName = "Concentration";
}

Concentration::~Concentration()
{
}

void Concentration::activate(Unit* caster)
{
	int dexAmount = 2;

	caster->dex += dexAmount;

	cout << "Dexterity increased by " << dexAmount << "!" << endl;
}
