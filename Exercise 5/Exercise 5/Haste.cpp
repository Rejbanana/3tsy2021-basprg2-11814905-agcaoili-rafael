#include "Haste.h"

Haste::Haste()
{
	this->skillName = "Haste";
}

Haste::~Haste()
{
}

void Haste::activate(Unit* caster)
{
	int agiAmount = 2;

	caster->agi += agiAmount;

	cout << "Agility increased by " << agiAmount << "!" << endl;
}
