#include <iostream>
#include <string>
#include <time.h>

#include "Unit.h"

#include "Skill.h"
#include "Heal.h"

using namespace std;

int main()
{
    srand(time(NULL));

    cout << "Hi there adventurer! What is your name? " << endl;
    string playerName;
    cin >> playerName;

    Unit* player = new Unit(playerName);

    system("cls");


    while (true) {
        cout << "Your current stats are: " << endl;
        player->printStats();

        cout << "Casting random skill..." << endl;
        system("pause");

        cout << endl;

        player->castRandSkill();
        system("pause");
        system("cls");
    }
}