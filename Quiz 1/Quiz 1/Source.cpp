#include <iostream>
#include <string>
#include <time.h>
#include <vector> 

using namespace std;


void initializePlayer(const vector<string>& vector, int& playerPick){
	

	for (int i = 0; i < vector.size(); i++)
	{
		cout << "[" << i+1 << "] " << vector[i] << endl;
		
	}
	cout << "pick a card you want to play: " << endl;

	cin >> playerPick;
	playerPick -= 1;


}
void initializeEnemy(const vector<string>& vector, int& enemyPick) {


	enemyPick = rand() % vector.size(); //enemy picking randomizer


}
string sideSwitch(string side) { //for switching sides

	string sides[2] = { "Emperor", "Slave" };
	if (side == sides[0]) {
		side = sides[1];
	}
	else {
		side = sides[0];
	}
	return side;
}

void populateVector(vector<string>& vector, string side) {

	string cardType = "Civilian";
	for (int i = 0; i < 5; i++){ 
		if (i == 0) {
			vector.push_back(side); //since only one card is your card "side
		}
		else {
			vector.push_back(cardType);
		}
	}
	
}


void deleteVector(vector<string>& vector, int pick) {
	vector.erase(vector.begin() + (pick));
}

void evaluate(int& money, int& mm, int playerBet, vector<string>& playerCards, int playerPick, int enemyPick, vector<string>& enemyCards){

	while (true) {

		initializePlayer(playerCards, playerPick);
		initializeEnemy(enemyCards, enemyPick);

		system("CLS");

		cout << "open" << endl << endl;


		cout << "[Kaiji] " << playerCards[playerPick] << " vs [Tonegawa] " << enemyCards[enemyPick] << endl << endl;

		if (playerCards[playerPick] == "Civilian") {
			if (enemyCards[enemyPick] == "Slave") {
				//win
				cout << "win" << endl;
				money += (playerBet * 100000);
				cout << "you won " << money;

				break;
			}
			else if (enemyCards[enemyPick] == "Emperor") {
				//lose
				cout << "lose" << endl;
				mm -= playerBet;
				break;
			}
			else if (enemyCards[enemyPick] == "Civilian") {
				//draw
				deleteVector(enemyCards, enemyPick); //deletes both civilian picks
				deleteVector(playerCards, playerPick);

				cout << "draw" << endl;
				system("pause");
				system("CLS");

			}



		}
		else if (playerCards[playerPick] == "Emperor") {
			if (enemyCards[enemyPick] == "Slave") {
				//lose
				cout << "lose" << endl;
				mm -= playerBet;
				break;
			}
			else if (enemyCards[enemyPick] == "Civilian") {
				//win
				cout << "win" << endl;
				money += (playerBet * 100000);
				cout << "you won " << money;
				break;
			}



		}
		else if (playerCards[playerPick] == "Slave") {
			if (enemyCards[enemyPick] == "Civilian") {
				//lose
				cout << "lose" << endl;
				break;
			}
			else if (enemyCards[enemyPick] == "Emperor") {
				//win
				cout << "win" << endl;

				money += (playerBet * 500000);
				cout << "you won " << money;
				break;
			}


		}

	}
}

void playRound(int& money, int& mm, int round, string side) { 
	int playerBet = 0;
	int playerPick = 0;
	int enemyPick = 0;
	string enemySide = sideSwitch(side); // makes sure that opponent is the side you arent
	
	vector<string> playerCards;
	vector<string> enemyCards;
	


	cout << "enemySide: " << enemySide;
	cout << endl;

	cout << "How many mm would you like to wager, Kaiji? ";
	while (playerBet == 0 or playerBet > mm or playerBet < 0) {
		cin >> playerBet;
	}

	
	system("CLS");


	populateVector(playerCards, side); // populates hand with cards 
	populateVector(enemyCards, enemySide);
	
	evaluate(money, mm, playerBet, playerCards, playerPick, enemyPick, enemyCards);
	

	

	cout << endl;
	cout << endl;
	system("pause");
	system("CLS");
	
	



}


int main()
	{

	srand(time(0));

	
	int round = 1;
	int mmLeft = 30;
	int moneyEarned = 0;
	string side = "Emperor";
	
	while (mmLeft > 0 and round < 13)
	{

		
		

		

		if ((round - 1) % 3 == 0 and round != 1) {
			side = sideSwitch(side);
			
		}
		cout << "cash: " << moneyEarned << endl;
		cout << "distance left: " << mmLeft << endl;
		cout << "round: " << round << "/12" << endl;
		cout << "side:" << side << endl;
	

		playRound(moneyEarned, mmLeft, round, side);
	
		if (moneyEarned >= 20000000) {
			break;
		}
		if (mmLeft == 0) {
			break;
		}
		round++;

	}
	if (mmLeft <= 0) { // bad ending
		cout << "oh no, you lost ur ear hehehe";
	}
	else if (moneyEarned >= 20000000) { // best ending
		cout << "yay you beat him";
	}
	else { // meh ending
		cout << "you have gotten the meh ending, nothing was gained, and you lost your pride";
	}

	cout << endl;
	cout << endl;

	system("pause");
	return 0;
}
