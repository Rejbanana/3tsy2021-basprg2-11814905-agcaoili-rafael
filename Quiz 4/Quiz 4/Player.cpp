#include "Player.h"
#include "Action.h"

Player::Player(string name) : Unit(name)
{
	this->team = "Warcraft";
}

Player::~Player()
{

}

void Player::combat(vector<Unit*> allyTeam, vector<Unit*> enemyTeam)
{
	this->printStats();
	cout << endl;

	cout << "Choose an action..." << endl;
	cout << "====================================" << endl;
	cout << "[1] " << actionList[0]->actionName << " (MP Cost: " << actionList[0]->mpCost << ")" << endl;
	cout << "[2] " << actionList[1]->actionName << " (MP Cost: " << actionList[1]->mpCost << ")" << endl;

	int choice = 0;
	while (true){
		cin >> choice;
		if (choice > 0 && choice < 3) {
			break;
		}
	}

	system("cls");

	if (this->mp >= actionList[choice - 1]->mpCost){
		this->mp -= actionList[choice - 1]->mpCost;

		actionList[choice - 1]->doAction(this, allyTeam, enemyTeam);
	}
	else{
		cout << "Not enough MP to cast " << actionList[1]->actionName << endl;
		actionList[0]->doAction(this, allyTeam, enemyTeam);
	}
}
