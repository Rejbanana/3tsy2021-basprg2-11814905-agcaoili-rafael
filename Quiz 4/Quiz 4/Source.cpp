#include <iostream>
#include <conio.h>
#include <string>
#include <time.h>
#include <vector>

#include "Unit.h"
#include "Player.h"
#include "Enemy.h"

#include "Action.h"
#include "BasicAttack.h"
#include "SingleTarget.h"
#include "MultiTarget.h"
#include "Heal.h"

using namespace std;

vector<Unit*> arrangeTurnOrder(vector<Unit*> turnOrder)
{
    for (int i = 0; i < turnOrder.size(); i++) {
        for (int j = i + 1; j < turnOrder.size(); j++) {
            if (turnOrder[i]->agi < turnOrder[j]->agi) {
                swap(turnOrder[i], turnOrder[j]);
            }
        }
    }

    return turnOrder;
}

void printTurnOrder(vector<Unit*> turnOrder)
{
    cout << "=========== TURN ORDER ===========" << endl;
    for (int i = 0; i < turnOrder.size(); i++) {
        cout << "#" << i + 1 << " [" << turnOrder[i]->team << "] " << turnOrder[i]->playerName << endl;
    }
    cout << "=========== TURN ORDER ===========" << endl;

    cout << endl;
}

void printTeam(vector<Unit*> teamList)
{
    cout << "==================================" << endl;
    cout << "Team: " << teamList[0]->team << endl;
    cout << "==================================" << endl;
    for (int i = 0; i < teamList.size(); i++) {
        cout << teamList[i]->playerName << " [HP: " << teamList[i]->hp << "]" << endl;
    }

    cout << endl;
}

vector<Unit*> removeIfDead(vector<Unit*> characterList)
{
    for (int i = 0; i < characterList.size(); i++) {
        if (characterList[i]->hp <= 0) {
            characterList.erase(characterList.begin() + i);
        }
    }

    return characterList;
}

vector<Unit*> adjustTurnOrder(vector<Unit*> turnOrder)
{
    turnOrder.push_back(turnOrder[0]);
    turnOrder.erase(turnOrder.begin() + 0);

    return turnOrder;
}

int main()
{
    srand(time(NULL));

    Player* player1 = new Player("E.T.C");
    player1->addAction(new MultiTarget("Face Melt"));

    Player* player2 = new Player("Illidan");
    player2->addAction(new SingleTarget("The Hunt"));

    Player* player3 = new Player("LiLi");
    player3->addAction(new Heal("Jug of Life"));

    Enemy* enemy1 = new Enemy("Johanna");
    enemy1->addAction(new MultiTarget("Face Melt"));

    Enemy* enemy2 = new Enemy("Valla");
    enemy2->addAction(new SingleTarget("Puncturing Arrow"));

    Enemy* enemy3 = new Enemy("Kharazim");
    enemy3->addAction(new Heal("Heal"));

    vector<Unit*> playerTeam;
    playerTeam.push_back(player1);
    playerTeam.push_back(player2);
    playerTeam.push_back(player3);

    vector<Unit*> enemyTeam;
    enemyTeam.push_back(enemy1);
    enemyTeam.push_back(enemy2);
    enemyTeam.push_back(enemy3);

    vector<Unit*> turnOrder;
    turnOrder.push_back(player1);
    turnOrder.push_back(player2);
    turnOrder.push_back(player3);
    turnOrder.push_back(enemy1);
    turnOrder.push_back(enemy2);
    turnOrder.push_back(enemy3);

    turnOrder = arrangeTurnOrder(turnOrder);

    while (playerTeam.empty() != true && enemyTeam.empty() != true) {
        printTeam(playerTeam);
        printTeam(enemyTeam);
        printTurnOrder(turnOrder);

        cout << "Current Turn: " << " [" << turnOrder[0]->team << "] " << turnOrder[0]->playerName << endl;

        system("pause");
        system("cls");

        if (turnOrder[0]->team == playerTeam[0]->team) {
            turnOrder[0]->combat(playerTeam, enemyTeam);
        }
        else if (turnOrder[0]->team == enemyTeam[0]->team) {
            turnOrder[0]->combat(enemyTeam, playerTeam);
        }

        playerTeam = removeIfDead(playerTeam);
        enemyTeam = removeIfDead(enemyTeam);
        turnOrder = removeIfDead(turnOrder);

        turnOrder = adjustTurnOrder(turnOrder);

        system("pause");
        system("cls");
    }

    if (enemyTeam.empty() == true) { // player wins
        cout << "Congratulations you wiped out their whole squad! You win!" << endl;
    }
    else if (playerTeam.empty() == true) { // enemy wins
        cout << "Your whole team got wiped out. You lose." << endl;
    }

    cout << endl << "Game Over." << endl;

    system("pause");
}