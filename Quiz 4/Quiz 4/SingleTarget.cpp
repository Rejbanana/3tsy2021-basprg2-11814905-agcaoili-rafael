#include "SingleTarget.h"

SingleTarget::SingleTarget(string name) : Action(name)
{
	this->mpCost = 5;
	this->dmgCoef = 2.2f;
}

SingleTarget::~SingleTarget()
{
}

void SingleTarget::doAction(Unit* caster, vector<Unit*> allyTeam, vector<Unit*> enemyTeam)
{
	int randEnemy = rand() % enemyTeam.size();
	cout << caster->playerName << " used " << this->actionName << " against " << enemyTeam[randEnemy]->playerName << "!" << endl;

	int randomizedPow = (rand() % (int)(caster->pow * 0.2f)) + caster->pow;
	int baseDmg = randomizedPow * this->dmgCoef;
	int damage = baseDmg - enemyTeam[randEnemy]->vit;

	if (damage < 1) {
		damage = 1;
	}

	cout << caster->playerName << " dealt " << damage << " damage." << endl << endl;

	enemyTeam[randEnemy]->hp -= damage;

	if (enemyTeam[randEnemy]->hp <= 0)
	{
		cout << enemyTeam[randEnemy]->playerName << " has died!" << endl << endl;
	}
}
