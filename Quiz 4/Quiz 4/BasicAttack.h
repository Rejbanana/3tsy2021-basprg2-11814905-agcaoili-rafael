#pragma once
#include "Action.h"
class BasicAttack : public Action
{
public:
	BasicAttack(string name);
	~BasicAttack();

	void doAction(Unit* caster, vector<Unit*> allyTeam, vector<Unit*> enemyTeam);
};

