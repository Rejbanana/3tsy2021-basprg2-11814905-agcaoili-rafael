#pragma once
#include "Action.h"
class SingleTarget : public Action
{
public: 
	SingleTarget(string name);
	~SingleTarget();

	void doAction(Unit* caster, vector<Unit*> allyTeam, vector<Unit*> enemyTeam);
};

