#include "Enemy.h"
#include "Action.h"

Enemy::Enemy(string name) : Unit(name)
{
	this->team = "Diablo";
}

Enemy::~Enemy()
{
}

void Enemy::combat(vector<Unit*> allyTeam, vector<Unit*> enemyTeam)
{
	system("cls");

	int choice = rand() % 2;

	if (this->mp > actionList[choice]->mpCost) {

		this->mp -= actionList[choice]->mpCost;

		actionList[choice]->doAction(this, allyTeam, enemyTeam);
	}
	else {
		actionList[0]->doAction(this, allyTeam, enemyTeam);
	}
}
