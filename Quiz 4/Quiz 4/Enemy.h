#pragma once
#include "Unit.h"
class Enemy : public Unit
{
public:
	Enemy(string name);
	~Enemy();

	void combat(vector<Unit*> allyTeam, vector<Unit*> enemyTeam);
};

