#pragma once
#include <string>
#include <iostream>
#include <vector>

using namespace std;
class Action;
class Unit
{
public: 
	Unit(string name);
	~Unit();

	string playerName;
	int maxHP;
	int hp;
	int mp;
	int pow;
	int vit;
	int dex;
	int agi;
	string team;
	vector<Action*> actionList;

	void addAction(Action* action);
	void printStats();
	virtual void combat(vector<Unit*> allyTeam, vector<Unit*> enemyTeam);
};

