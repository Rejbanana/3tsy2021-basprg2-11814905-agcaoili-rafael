#pragma once
#include <string>
#include <iostream>
#include "Unit.h"

using namespace std;

class Action
{
public:
	Action(string name);
	~Action();

	string actionName;
	int mpCost;
	float dmgCoef;

	virtual void doAction(Unit* caster, vector<Unit*> allyTeam, vector<Unit*> enemyTeam);
};

