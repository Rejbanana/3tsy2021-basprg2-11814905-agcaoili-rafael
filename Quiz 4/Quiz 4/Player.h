#pragma once
#include "Unit.h"
class Player : public Unit
{
public:
	Player(string name);
	~Player();

	void combat(vector<Unit*> allyTeam, vector<Unit*> enemyTeam);
};

