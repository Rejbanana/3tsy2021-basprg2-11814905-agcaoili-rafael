#include "Unit.h"
#include "Action.h"
#include "BasicAttack.h"

Unit::Unit(string name)
{
	this->playerName = name;
	this->maxHP = 100;
	this->hp = maxHP;
	this->mp = (rand() % 10 + 1) + 10;
	this->pow = (rand() % 10 + 1) + 40;
	this->vit = (rand() % 10 + 1) + 30;
	this->dex = (rand() % 20 + 1) + 50;
	this->agi = (rand() % 30 + 1) + 40;

	this->addAction(new BasicAttack("Basic Attack"));
}

Unit::~Unit()
{
}

void Unit::addAction(Action* action)
{
	this->actionList.push_back(action);
}

void Unit::printStats()
{
	cout << "Name: " << this->playerName << endl;
	cout << "Team: " << this->team << endl;
	cout << "HP: " << this->hp << endl;
	cout << "MP: " << this->mp << endl;
	cout << "Pow: " << this->pow << endl;
	cout << "Vit: " << this->vit << endl;
	cout << "Dex: " << this->dex << endl;
	cout << "Agi: " << this->agi << endl;
}

void Unit::combat(vector<Unit*> allyTeam, vector<Unit*> enemyTeam)
{
	
}
