#pragma once
#include "Action.h"
class Heal : public Action
{
public: 
	Heal(string name);
	~Heal();

	void doAction(Unit* caster, vector<Unit*> allyTeam, vector<Unit*> enemyTeam);
};

