#include "Heal.h"

Heal::Heal(string name) : Action(name)
{
	this->mpCost = 3;
	this->dmgCoef = 0;
}

Heal::~Heal()
{
}

void Heal::doAction(Unit* caster, vector<Unit*> allyTeam, vector<Unit*> enemyTeam)
{
	int lowestHPIndex = 0;
	for (int i = 1; i < allyTeam.size(); i++){
		if (allyTeam[lowestHPIndex]->hp > allyTeam[i]->hp) {
			lowestHPIndex = i;
		}
			
	}

	cout << caster->playerName << " used " << this->actionName << " to heal " << allyTeam[lowestHPIndex]->playerName << "!" << endl;

	int healAmount = allyTeam[lowestHPIndex]->maxHP * 0.3;

	cout << allyTeam[lowestHPIndex]->playerName << " was healed for " << healAmount << " HP!" << endl;

	allyTeam[lowestHPIndex]->hp += healAmount;
	if (allyTeam[lowestHPIndex]->hp > allyTeam[lowestHPIndex]->maxHP) {
		allyTeam[lowestHPIndex]->hp = allyTeam[lowestHPIndex]->maxHP;
	}
}
