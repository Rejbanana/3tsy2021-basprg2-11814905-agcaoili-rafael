#include "BasicAttack.h"

BasicAttack::BasicAttack(string name) : Action(name)
{
	this->mpCost = 0;
	this->dmgCoef = 1.0f;
}

BasicAttack::~BasicAttack()
{
}

void BasicAttack::doAction(Unit* caster, vector<Unit*> allyTeam, vector<Unit*> enemyTeam)
{
	int randEnemy = rand() % enemyTeam.size();
	cout << caster->playerName << " used " << this->actionName << " against " << enemyTeam[randEnemy]->playerName << "!" << endl;
	
	int hitRate = (caster->dex / enemyTeam[randEnemy]->agi) * 100;

	if (hitRate < 20){
		hitRate = 20;
	}
	else if (hitRate > 80){
		hitRate = 80;
	}

	int hitChance = rand() % 100 + 1;

	if (hitRate > hitChance){
		int randomizedPow = (rand() % (int)(caster->pow * 0.2f)) + caster->pow;
		int baseDmg = randomizedPow * this->dmgCoef;
		int damage = baseDmg - enemyTeam[randEnemy]->vit;

		if (damage < 1) {
			damage = 1;
		}

		cout << caster->playerName << " dealt " << damage << " damage." << endl;

		enemyTeam[randEnemy]->hp -= damage;

		if (enemyTeam[randEnemy]->hp <= 0)
		{
			cout << enemyTeam[randEnemy]->playerName << " has died!" << endl << endl;
		}
	}
	else
	{
		cout << "the Attack misses." << endl;
	}
}
