#pragma once
#include "Action.h"
class MultiTarget : public Action
{
public: 
	MultiTarget(string name);
	~MultiTarget();

	void doAction(Unit* caster, vector<Unit*> allyTeam, vector<Unit*> enemyTeam);
};

