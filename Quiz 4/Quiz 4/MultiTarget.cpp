#include "MultiTarget.h"

MultiTarget::MultiTarget(string name) : Action(name)
{
	this->mpCost = 4;
	this->dmgCoef = 0.9f;
}

MultiTarget::~MultiTarget()
{
}

void MultiTarget::doAction(Unit* caster, vector<Unit*> allyTeam, vector<Unit*> enemyTeam)
{
	cout << caster->playerName << " used " << this->actionName << " against all opponents." << endl;

	for (int i = 0; i < enemyTeam.size(); i++) {
		int randomizedPow = (rand() % (int)(caster->pow * 0.2f)) + caster->pow;
		int baseDmg = randomizedPow * this->dmgCoef;
		int damage = baseDmg - enemyTeam[i]->vit;

		if (damage < 1) {
			damage = 1;
		}

		cout << caster->playerName << " dealt " << damage << " damage against " << enemyTeam[i]->playerName << endl;

		enemyTeam[i]->hp -= damage;

		if (enemyTeam[i]->hp <= 0)
		{
			cout << enemyTeam[i]->playerName << " has died!" << endl << endl;
		}
	}
}
