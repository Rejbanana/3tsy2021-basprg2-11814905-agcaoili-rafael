#pragma once
#include <string>

using namespace std;

class Unit
{
public:
	Unit();

	string playerName;
	string className;
	int maxHP;
	int currentHP;
	int pow;
	int vit;
	int agi;
	int dex;

	void setPlayerName(string name);
	void setAsWarrior();
	void setAsAssassin();
	void setAsMage();
	void printStats();
	int calculateHitRate(Unit* target);
	int calculateDamage(Unit* target);
	void attack(Unit* target);
};

#pragma once
