// Quiz3_Agcaoili.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <time.h>
#include "Unit.h"

void createRandomEnemy(Unit* enemy)
{
    int randNum = rand() % 3 + 1;

    if (randNum == 1) {
        enemy->setPlayerName("Enemy Warrior");
        enemy->setAsWarrior();
    }
    else if (randNum == 2) {
        enemy->setPlayerName("Enemy Assassin");
        enemy->setAsAssassin();
    }
    else if (randNum == 3) {
        enemy->setPlayerName("Enemy Mage");
        enemy->setAsMage();
    }
    else {
        cout << "Error" << endl;
    }

    enemy->pow -= 2; //This is just to nerf the enemy
    enemy->vit -= 2;
    enemy->agi -= 1;
    enemy->dex -= 2;
}

void addWinBonus(Unit* player, Unit* enemy)
{
    if (enemy->className == "Warrior") {
        player->pow += 3;
        player->vit += 3;
    }
    else if (enemy->className == "Assassin") {
        player->agi += 3;
        player->dex += 3;
    }
    else if (enemy->className == "Mage") {
        player->pow += 5;
    }

    player->currentHP += player->maxHP * .3;
    if (player->currentHP > player->maxHP) {
        player->currentHP = player->maxHP;
    }
}

void printWinBonus(Unit* player, Unit* enemy)
{
    int hp = 0;
    int pow = 0;
    int vit = 0;
    int agi = 0;
    int dex = 0;

    if (enemy->className == "Warrior") {
        pow += 3;
        vit += 3;
    }
    else if (enemy->className == "Assassin") {
        agi += 3;
        dex += 3;
    }
    else if (enemy->className == "Mage") {
        pow += 5;
    }

    cout << "Combat ended..." << endl;
    cout << "Increased stats by..." << endl;
    cout << "HP: " << hp << endl;
    cout << "Pow: " << pow << endl;
    cout << "Vit: " << vit << endl;
    cout << "Agi: " << agi << endl;
    cout << "Dex: " << dex << endl;

    int hpHealed = player->maxHP * .3;
    cout << endl << "You are healed for " << hpHealed << " HP" << endl;
    system("pause");
}

void addEnemyBonus(Unit* enemy, int stageNum)
{
    int enemyBonus = stageNum + 1;
    enemy->pow += enemyBonus;
    enemy->vit += enemyBonus;
    enemy->agi += enemyBonus;
    enemy->dex += enemyBonus;

}

int main()
{
    srand(time(NULL));

    Unit* player = new Unit();
    string playerName;
    cout << "Enter the name of your character: ";
    cin >> playerName;
    player->setPlayerName(playerName);

    system("cls");

    int classChoice;
    while (true) {
        cout << "choose your class: " << endl;
        cout << "[1] Warrior" << endl;
        cout << "[2] Assassin" << endl;
        cout << "[3] Mage" << endl;
        cin >> classChoice;

        system("cls");
        if (classChoice >= 1 && classChoice <= 3) {
            break;
        }
    }

    if (classChoice == 1) {
        player->setAsWarrior();
    }
    else if (classChoice == 2) {
        player->setAsAssassin();
    }
    else if (classChoice == 3) {
        player->setAsMage();
    }
    else {
        cout << "Error" << endl;
    }

    int stageNum = 1;

    Unit* enemy = new Unit();
    while (player->currentHP > 0) {
        createRandomEnemy(enemy);
        if (stageNum > 1) {
            addEnemyBonus(enemy, stageNum); //makes enemies stronger after each stage
        }

        cout << "Stage: " << stageNum << endl;
        player->printStats();
        cout << endl << "VS" << endl << endl;
        enemy->printStats();
        cout << endl << "Initiating Combat..." << endl;
        system("pause");
        system("cls");

        while (player->currentHP > 0 && enemy->currentHP > 0) { //battle loop
            if (player->agi > enemy->agi) {
                player->attack(enemy);
                system("pause");

                if (enemy->currentHP > 0) {
                    enemy->attack(player);
                    system("pause");
                }
            }
            else {
                enemy->attack(player);
                system("pause");

                if (player->currentHP > 0) {
                    player->attack(enemy);
                    system("pause");
                }
            }
            system("cls");
        }

        if (enemy->currentHP <= 0) {
            system("cls");
            addWinBonus(player, enemy); //adds the win bonus and heals if the player wins
            printWinBonus(player, enemy); //prints win bonus
            stageNum++;
        }
        system("cls");
    }

    if (player->currentHP < 0)
    {
        player->currentHP = 0;
    }

    cout << "Stage reached: " << stageNum << endl;
    cout << "Player Stats: " << endl;
    player->printStats();

    delete player; //deallocating objects
    delete enemy;
}
