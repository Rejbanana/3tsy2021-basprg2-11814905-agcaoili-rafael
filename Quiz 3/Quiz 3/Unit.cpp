#include "Unit.h"
#include <iostream>

Unit::Unit()
{
	this->playerName = "";
	this->className = "";
	this->maxHP = 0;
	this->currentHP = maxHP;
	this->pow = 0;
	this->vit = 0;
	this->agi = 0;
	this->dex = 0;
}

void Unit::setPlayerName(string name)
{
	this->playerName = name;
}

void Unit::setAsWarrior()
{
	this->className = "Warrior";
	this->maxHP = 20;
	this->currentHP = maxHP;
	this->pow = 10;
	this->vit = 6;
	this->agi = 3;
	this->dex = 5;
}

void Unit::setAsAssassin()
{
	this->className = "Assassin";
	this->maxHP = 18;
	this->currentHP = maxHP;
	this->pow = 9;
	this->vit = 4;
	this->agi = 5;
	this->dex = 7;
}

void Unit::setAsMage()
{
	this->className = "Mage";
	this->maxHP = 15;
	this->currentHP = maxHP;
	this->pow = 12;
	this->vit = 4;
	this->agi = 3;
	this->dex = 4;
}

void Unit::printStats()
{
	cout << "Name: " << this->playerName << endl;
	cout << "Class: " << this->className << endl;
	cout << "HP: " << this->currentHP << "/" << this->maxHP << endl;
	cout << "Pow: " << this->pow << endl;
	cout << "Vit: " << this->vit << endl;
	cout << "Agi: " << this->agi << endl;
	cout << "Dex: " << this->dex << endl;
}

int Unit::calculateHitRate(Unit* target)
{
	int hitRate = (this->dex / target->agi) * 100;

	if (hitRate < 20){
		hitRate = 20;
	}
	else if (hitRate > 80) {
		hitRate = 80;
	}

	return hitRate;
}

int Unit::calculateDamage(Unit* target)
{
	int damageBonus = 1;
	if ((this->className == "Warrior" && target->className == "Assassin") ||
		(this->className == "Assassin" && target->className == "Mage") ||
		(this->className == "Mage" && target->className == "Warrior")) { //checks for damage bonus
		damageBonus = 1.5;

		cout << "crit!" << endl; //just to check if the bonus is applied
	}
	int damage = (this->pow - target->vit) * damageBonus;

	if (damage < 1) {
		damage = 1;
	}

	return damage;
}

void Unit::attack(Unit* target)
{
	cout << this->playerName << " attacks " << target->playerName << "!" << endl;
	int hitChance = rand() % 100 + 1;
	if (this->calculateHitRate(target) > hitChance) { //checks if the attack hits
		int damage = this->calculateDamage(target);
		cout << this->playerName << " dealt " << damage << " damage to " << target->playerName << "!" << endl;
		target->currentHP -= damage;
		if (target->currentHP <= 0)
		{
			cout << target->playerName << " has died!" << endl;
		}
	}
	else
	{
		cout << this->playerName << " missed!" << endl;
	}
}
